# [Cultured Code - _Tasks sample project refactor_](https://cultured-code.breezy.hr/p/464ee8c1ade901-ios-macos-developer)

## Refactor sample project progress

[**Done**] Objective-C
[**Done**] Swift

## Cover letter

[Markdown](docs_raw/cover_letter.md)
[Pdf](jozsef_csaszi_cl.pdf)

## Resume

[Docx](docs_raw/curriculum_vitae.docx)
[Pdf](jozsef_csaszi_cv.pdf)
[LinkedIn](https://www.linkedin.com/in/j%C3%B3zsef-kriszti%C3%A1n-cs%C3%A1szi-6a242a70/)

## References

I'm not able to share code-snippets or project/framework code, because of the privacy statement. But, I can tell about the projects I worked on and list the publicly available _(youtube)_ videos according to the projects _(just be sure to check the video upload date, the videos didn't update frequently)_:

### 2020 _(Objective-C)_

This is the mobile team's main project. We put everything we can, like: today widget, siri extension, share extension, siri shortcut _(new)_, VR _(Unity)_, AR _(Nearest branch & atm finder)_ and many other ideas what came from our innovation managers.

**PFA** (Personal Financial Assistant):

- https://www.youtube.com/watch?v=zk-Dcx9R92Y
- https://www.youtube.com/watch?v=QR77i3D5dTA

**Google assistant**:

- https://www.youtube.com/watch?v=lFN1-h-2fBY

**Biometric authentication** (Iris scan):

- https://www.youtube.com/watch?v=6FYms3qV3eQ

**Use Siri to send money:**

- https://www.youtube.com/watch?v=cw1eL-lwy1o

**Wishes and Goals** redefined:

- https://www.youtube.com/watch?v=Hexm18oCq2s

### Maneko _(Swift)_

It's an experimental B2C application to solve the following challenge: "How might we encourage young adults to live a sustainable financial life?". In short: a smart-saving manager & adviser application.

- https://youtu.be/CPVGlF3YToc?t=271
- (user-test) https://www.youtube.com/watch?v=yirDAaNTDBI

### ConvoEngine _(Swift)_

Developed only by me.  
It's a static-framework shared between **2020** & **Maneko**.  
Responsible for Conversation model-parsing, flow-handling & Conversation related state handling.

### CryptoWeather _(NodeJS)_

The projects main goal is to **predict** the **BTC/USD price** for the next day. I am working on this project in a 3 man group (1 manager, 1 data-scientist and me as "API" developer). The project uses twitter, google-trends and a few 3rd-party data sources.  
My main task is to prepare a historical dataset and store it each day. After the preprocessing is done, the service makes the prediction with multiple models and dataset combinations. When the prediction workers finished, we select the "best" one and tweet it.  
Also the "bot" tweets the yesterday's prediction comparison results and on daily basis tweets an actual/1d/7d/30d price change message.

https://twitter.com/cryptoweather_
