//
//  TaskCell.swift
//  TasksSwift
//
//  Created by Jace on 8/5/18.
//  Copyright © 2018 Cultured Code GmbH & Co. KG. All rights reserved.
//

import Foundation
import UIKit

private let checkboxEmpty = UIImage(named: "ic_checkbox_empty")
private let checkboxChecked = UIImage(named: "ic_checkbox_checked")

class TaskCell: UITableViewCell {
    private lazy var container: UIView = setupContainerView()
    private lazy var checkmark: UIImageView = setupCheckmarkImageView()
    private lazy var title: UILabel = setupTitleLabel()

    private var task: TSTask? {
        didSet {
            self.textLabel?.text = task?.title
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupViews()
    }

    func configure(with task: TSTask) {
        accessoryType = (task.childrenTasks.isEmpty ? .none : .detailDisclosureButton)
        checkmark.image = (task.complete ? checkboxChecked : checkboxEmpty)
        title.text = task.title
        title.textColor = (task.complete ? .lightGray : .black)

        // Configure accessibility elements
        container.accessibilityLabel = task.title
        let locKeyAcValue = (task.complete ? "tasks_list_element_checked" : "tasks_list_element_empty")
        container.accessibilityValue = NSLocalizedString(locKeyAcValue,
                                                         comment: "Accessibility value for a Task")
    }

    // MARK: - Setup UI elements

    private func setupContainerView() -> UIView {
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = .clear
        container.isAccessibilityElement = true
        container.accessibilityTraits = UIAccessibilityTraitButton
        container.accessibilityHint = NSLocalizedString("tasks_list_element_ac_hint",
                                                        comment: "Accessibility hint for Task tap")

        return container
    }

    private func setupCheckmarkImageView() -> UIImageView {
        let checkmark = UIImageView(image: checkboxEmpty)
        checkmark.translatesAutoresizingMaskIntoConstraints = false

        return checkmark
    }

    private func setupTitleLabel() -> UILabel {
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.numberOfLines = 1

        let font = UIFont(name: "AmericanTypewriter", size: 16.0)
        // Using Custom Font with Dynamic Type
        if let font = font {
            let fontMetrics = UIFontMetrics(forTextStyle: .body)
            titleLabel.font = fontMetrics.scaledFont(for: font)
            titleLabel.adjustsFontForContentSizeCategory = true
        }

        return titleLabel
    }

    private func setupViews() {
        var constraints = [NSLayoutConstraint]()
        contentView.addSubview(container)
        container.addSubview(checkmark)
        container.addSubview(title)

        // ContainerView
        constraints += [container.topAnchor.constraint(equalTo: contentView.topAnchor),
                        container.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                        container.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
                        container.leadingAnchor.constraint(equalTo: contentView.leadingAnchor)]
        // Checkmark ImageView
        constraints += [checkmark.topAnchor.constraint(equalTo: container.topAnchor),
                        checkmark.bottomAnchor.constraint(equalTo: container.bottomAnchor),
                        checkmark.leadingAnchor.constraint(equalTo: container.leadingAnchor),
                        checkmark.widthAnchor.constraint(equalToConstant: 40.0)]
        // Title Label
        constraints += [title.leadingAnchor.constraint(equalTo: checkmark.trailingAnchor),
                        title.centerYAnchor.constraint(equalTo: container.centerYAnchor)]

        NSLayoutConstraint.activate(constraints)
    }
}
