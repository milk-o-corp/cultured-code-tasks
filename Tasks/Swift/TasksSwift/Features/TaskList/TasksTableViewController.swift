//
//  TasksTableViewController.swift
//  TasksSwift
//
//  Created by Jace on 8/5/18.
//  Copyright © 2018 Cultured Code GmbH & Co. KG. All rights reserved.
//

import Foundation
import UIKit

private let cellIdentifier = "TaskCell"

final class TasksTableViewController: UITableViewController {
    private var tasks: [TSTask]

    init(tasks: [TSTask]) {
        self.tasks = tasks

        super.init(style: UITableViewStyle.plain)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if tasks.first?.parentTask == nil {
            title = NSLocalizedString("tasks_title", comment: "Tasks navigation title")
        }
        navigationController?.navigationBar.prefersLargeTitles = true
        toolbarItems = [UIBarButtonItem.init(title: NSLocalizedString("tasks_tb_complete_all",
                                                                      comment: "Tasks action to mark all not-completed task as complete"),
                                             style: UIBarButtonItemStyle.plain,
                                             target: self,
                                             action: #selector(TasksTableViewController.completeAll)),
                        UIBarButtonItem.init(title: NSLocalizedString("tasks_tb_sort_by_name",
                                                                      comment: "Tasks action to sort the tasks by name"),
                                             style: UIBarButtonItemStyle.plain,
                                             target: self,
                                             action: #selector(TasksTableViewController.sort))
        ]

        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        tableView.register(TaskCell.self, forCellReuseIdentifier: cellIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - TableView DataSource

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TaskCell else {
            fatalError("Inconsistent cell registration")
        }

        let task = tasks[indexPath.row]
        cell.configure(with: task)

        return cell
    }

    // MARK: - TableView Delegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = tasks[indexPath.row]
        task.toggleComplete()

        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
    }

    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let task = tasks[indexPath.row]
        guard !task.childrenTasks.isEmpty else { return }

        let tvc = TasksTableViewController(tasks: task.childrenTasks)
        tvc.title = task.title
        navigationController?.pushViewController(tvc, animated: true)
    }

    // MARK: - Action handlers

    @objc private func completeAll() {
        let rowsToReload = tasks
            .filter({ !$0.complete })
            .compactMap({ tasks.index(of: $0) })
            .map({ IndexPath(row: $0, section: 0) })
        guard !rowsToReload.isEmpty else { return }

        tasks.completeAll()
        updateParentIfPossible()

        tableView.beginUpdates()
        tableView.reloadRows(at: rowsToReload, with: .fade)
        tableView.endUpdates()
    }

    @objc private func sort() {
        guard !tasks.isSorted() else { return }

        let tasksBeforeSort = tasks
        tasks.sortByTitle()
        updateParentIfPossible()

        let diff = [TSTask].diff(objects: tasks, originals: tasksBeforeSort)

        tableView.beginUpdates()
        for (idx, oldIndexPath) in diff.move.enumerated() {
            let newIndexPath = diff.destination[idx]
            tableView.moveRow(at: oldIndexPath, to: newIndexPath)
        }
        tableView.endUpdates()
    }

    // MARK: - Data Helpers

    private func updateParentIfPossible() {
        guard let parentTask = tasks.first?.parentTask else { return }
        parentTask.childrenTasks = tasks
    }
}
