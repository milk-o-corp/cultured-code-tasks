//
//  TSApplication.swift
//  TasksSwift
//
//  Created by Csaszi, Jozsef on 2018. 08. 28..
//  Copyright © 2018. Cultured Code GmbH & Co. KG. All rights reserved.
//

import Foundation
import UIKit

class TSApplication {
    static func setupMainInterface(in window: UIWindow) {
        let tasksTableViewController = TasksTableViewController(tasks: dummyTasks())
        let navigationController = UINavigationController(rootViewController: tasksTableViewController)
        navigationController.isToolbarHidden = false

        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    private static func dummyTasks() -> [TSTask] {
        let taskDescriptions: [[String: Any]] = [
            [ TSTask.Keys.title: "Buy milk",
              TSTask.Keys.complete: false ],

            [ TSTask.Keys.title: "Pay rent",
              TSTask.Keys.complete: false ],

            [ TSTask.Keys.title: "Change tires",
              TSTask.Keys.complete: false ],

            [ TSTask.Keys.title: "Sleep",
              TSTask.Keys.complete: false,
              TSTask.Keys.children: [
                    [ TSTask.Keys.title: "Find a bed",
                      TSTask.Keys.complete: false
                    ],

                    [ TSTask.Keys.title: "Lie down",
                      TSTask.Keys.complete: false
                    ],

                    [ TSTask.Keys.title: "Close eyes",
                      TSTask.Keys.complete: false
                    ],

                    [ TSTask.Keys.title: "Wait",
                      TSTask.Keys.complete: false
                    ]
                ] ],

            [ TSTask.Keys.title: "Dance",
              TSTask.Keys.complete: false ]
            ]

        return taskDescriptions.compactMap { TSTask(descriptor: $0) }
    }
}
