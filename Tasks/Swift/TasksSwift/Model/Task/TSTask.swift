//
//  TSTask.swift
//  TasksSwift
//
//  Created by Jace on 8/5/18.
//  Copyright © 2018 Cultured Code GmbH & Co. KG. All rights reserved.
//

import Foundation

final class TSTask {
    struct Keys {
        static let title = "title"
        static let complete = "complete"
        static let children = "children"
    }
    private(set) var complete: Bool = false {
        didSet {
            modified = Date()
        }
    }
    private(set) var title: String {
        didSet {
            modified = Date()
        }
    }
    var childrenTasks = [TSTask]() {
        didSet {
            modified = Date()
        }
    }
    private(set) var modified = Date()
    weak var parentTask: TSTask?

    init() {
        self.title = "<no title>"
    }

    init(title: String) {
        self.title = title
    }

    init?(descriptor: [String: Any]) {
        guard let title = descriptor[Keys.title] as? String, let complete = descriptor[Keys.complete] as? Bool else { return nil }

        self.title = title
        self.complete = complete
        if let childrenDescriptors = descriptor[Keys.children] as? [[String: Any]] {
            self.childrenTasks = childrenDescriptors.compactMap({
                let task = TSTask(descriptor: $0)
                task?.parentTask = self
                return task
            })
        }
    }

    // MARK: - Action helpers

    func toggleComplete() {
        complete.toggle()
    }

    // MARK: - Child manipulation

    func addChild(child: TSTask) {
        child.parentTask = self
        childrenTasks.append(child)
    }

    func removeChild(child: TSTask) {
        childrenTasks.remove(object: child)
    }

    func makeAllChildrenComplete() {
        childrenTasks.completeAll()
    }

    func deleteChildren() {
        childrenTasks.removeAll()
    }
}

extension TSTask: Equatable {
    static func == (lhs: TSTask, rhs: TSTask) -> Bool {
        return lhs.title == rhs.title
            && lhs.complete == rhs.complete
            && lhs.childrenTasks == rhs.childrenTasks
    }
}

extension TSTask: Comparable {
    static func < (lhs: TSTask, rhs: TSTask) -> Bool {
        return lhs.title.compare(rhs.title) == .orderedAscending
    }
}

extension Array where Element: TSTask {
    func completeAll() {
        self.filter({ !$0.complete }).forEach({ $0.toggleComplete() })
    }

    mutating func sortByTitle() {
        self.sort { $0.title < $1.title }
    }
}
