//
//  Bool+Extension.swift
//  TasksSwift
//
//  Created by Csaszi, Jozsef on 2018. 08. 29..
//  Copyright © 2018. Cultured Code GmbH & Co. KG. All rights reserved.
//

import Foundation

extension Bool {
    mutating func toggle() {
        self = !self
    }
}
