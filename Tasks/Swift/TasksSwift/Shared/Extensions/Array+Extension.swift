//
//  Array+Extension.swift
//  TasksSwift
//
//  Created by Csaszi, Jozsef on 2018. 08. 29..
//  Copyright © 2018. Cultured Code GmbH & Co. KG. All rights reserved.
//

import Foundation

typealias ArrayDiff = (delete: [IndexPath], insert: [IndexPath], move: [IndexPath], destination: [IndexPath])

extension Array where Element: Equatable {
    @discardableResult
    mutating func remove(object: Element) -> Bool {
        if let index = index(of: object) {
            self.remove(at: index)
            return true
        }
        return false
    }

    static func diff<T: Equatable>(objects: [T], originals: [T], section: Int = 0) -> ArrayDiff {
        var pathsToDelete = [IndexPath]()
        var pathsToInsert = [IndexPath]()
        var pathsToMove = [IndexPath]()
        var destinationPaths = [IndexPath]()

        // Deletes and moves
        for (oldIndex, object) in originals.enumerated() {
            if let newIndex = objects.index(of: object) {
                pathsToMove.append(IndexPath(row: oldIndex, section: section))
                destinationPaths.append(IndexPath(row: newIndex, section: section))
            } else {
                pathsToDelete.append(IndexPath(row: oldIndex, section: section))
            }
        }
        // Inserts
        for (newIndex, object) in objects.enumerated() {
            if originals.contains(object) {
                pathsToInsert.append(IndexPath(row: newIndex, section: section))
            }
        }

        return (pathsToDelete, pathsToInsert, pathsToMove, destinationPaths)
    }
}

extension Array where Element: Comparable {
    func isSorted() -> Bool {
        for (index, obj) in self.enumerated().dropFirst() where self[index - 1] > obj {
            return false
        }
        return true
    }
}
