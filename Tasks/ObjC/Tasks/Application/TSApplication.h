//
//  TSApplication.h
//  Tasks
//
//  Created by Csaszi, Jozsef on 2018. 08. 24..
//  Copyright © 2018. Cultured Code. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface TSApplication : NSObject

+(void)setupMainInterfaceIn:(UIWindow *)window;


@end
