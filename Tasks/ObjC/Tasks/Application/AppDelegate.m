//
//  AppDelegate.m
//  Tasks
//
//  Copyright (c) 2013 Cultured Code. All rights reserved.
//


#import "AppDelegate.h"
#import "TSApplication.h"


@implementation AppDelegate

-(BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [UIWindow.alloc initWithFrame:UIScreen.mainScreen.bounds];
    self.window.backgroundColor = UIColor.whiteColor;

    [TSApplication setupMainInterfaceIn:self.window];

    return true;
}


@end
