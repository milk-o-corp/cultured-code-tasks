//
//  AppDelegate.h
//  Tasks
//
//  Copyright (c) 2013 Cultured Code. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;


@end
