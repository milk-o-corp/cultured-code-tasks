//
//  TSApplication.m
//  Tasks
//
//  Created by Csaszi, Jozsef on 2018. 08. 24..
//  Copyright © 2018. Cultured Code. All rights reserved.
//


#import "TSApplication.h"
#import "TSTask.h"
#import "TasksTableViewController.h"


@implementation TSApplication

+(void)setupMainInterfaceIn:(UIWindow *)window
{
    UIViewController *tasksTableViewController = [TasksTableViewController.alloc initWithTasks:self.dummyTasks];
    UINavigationController *navigationController = [UINavigationController.alloc initWithRootViewController:tasksTableViewController];
    navigationController.toolbarHidden = false;

    window.rootViewController = navigationController;
    [window makeKeyAndVisible];
}


#pragma mark - Dummy data generators

+(NSArray *)dummyTasks
{
    NSArray *taskDescriptors =
    @[@{ kTaskTitle     : @"Buy milk",
         kTaskCompleted : @(false) },
      @{ kTaskTitle     : @"Pay rent",
         kTaskCompleted : @(false) },
      @{ kTaskTitle     : @"Change tires",
         kTaskCompleted : @(false) },
      @{ kTaskTitle     : @"Sleep",
         kTaskCompleted : @(false),
         kTaskChildrenTasks  : @[@{ kTaskTitle     : @"Find a bed",
                                    kTaskCompleted : @(false) },
                                 @{ kTaskTitle     : @"Lie down",
                                    kTaskCompleted : @(false) },
                                 @{ kTaskTitle     : @"Close eyes",
                                    kTaskCompleted : @(false) },
                                 @{ kTaskTitle     : @"Wait",
                                    kTaskCompleted : @(false) }
                                 ] },
      @{ kTaskTitle     : @"Dance",
         kTaskCompleted : @(false) }];

    return [self tasksFromDescriptors:taskDescriptors];
}

+(NSArray<TSTask *> *)tasksFromDescriptors:(NSArray *)taskDescriptors
{
    NSMutableArray *tasks = NSMutableArray.new;

    for (NSDictionary *taskDescription in taskDescriptors)
    {
        TSTask *task = [TSTask.alloc initWithTaskDescriptor:taskDescription];
        [tasks addObject:task];
    }
    return tasks;
}


@end
