//
//  TasksTableViewController.m
//  Tasks
//
//  Copyright (c) 2013 Cultured Code. All rights reserved.
//


#import "TasksTableViewController.h"
#import "TaskCell.h"

static NSString *cellIdentifier = @"TaskCell";


@implementation NSArray (Diff)

-(void)movingIndexPathsFor:(NSArray *)originalObjects
                  moveFrom:(NSArray<NSIndexPath *> *__autoreleasing*)moveFrom
                    moveTo:(NSArray<NSIndexPath *> *__autoreleasing*)moveTo
{
    NSMutableArray *pathsMoveFrom = NSMutableArray.new;
    NSMutableArray *pathsMoveTo = NSMutableArray.new;

    for (NSUInteger index = 0; index < self.count; index++)
    {
        id object = self[index];
        NSUInteger originalIndex = [originalObjects indexOfObject:object];
        if (originalIndex != index)
        {
            [pathsMoveFrom addObject:[NSIndexPath indexPathForRow:originalIndex
                                                        inSection:0]];
            [pathsMoveTo addObject:[NSIndexPath indexPathForRow:index
                                                      inSection:0]];
        }
    }
    *moveFrom = pathsMoveFrom;
    *moveTo = pathsMoveTo;
}

@end

@interface TasksTableViewController ()

@property (nonatomic, strong) NSArray<TSTask *> *tasks;

@end

@implementation TasksTableViewController

-(instancetype)initWithTasks:(NSArray<TSTask *> *)tasks
{
    if (!(self = [super initWithStyle:UITableViewStylePlain]))
    {
        return nil;
    }
    self.tasks = tasks;

    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.prefersLargeTitles = true;
    if (!self.title)
    {
        self.title = NSLocalizedString(@"tasks_title", @"Tasks navigation title");
    }
    [self setupToolbar];
    [self setupTableView];
}


#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView
numberOfRowsInSection:(NSInteger)section
{
    return self.tasks.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TaskCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                     forIndexPath:indexPath];
    [cell configureWith:self.tasks[indexPath.row]];

    return cell;
}


#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TSTask *task = self.tasks[indexPath.row];
    [task toggleComplete];

    [self reloadRows:@[indexPath]];
}

-(void)tableView:(UITableView *)tableView
accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    TSTask *task = self.tasks[indexPath.row];

    TasksTableViewController *tvc = [TasksTableViewController.alloc initWithTasks:task.childrenTasks];
    tvc.title = task.title;

    [self.navigationController pushViewController:tvc
                                         animated:true];
}


#pragma mark - Action handlers

-(void)completeAll
{
    NSArray *rowsToReload = [TSTask notCompletedIndexPathsFrom:self.tasks
                                                     inSection:0];
    if (rowsToReload.count == 0)
    {
        return;
    }
    [TSTask makeComplete:self.tasks];

    [self reloadRows:rowsToReload];
    [self updateParentTaskIfPossible];
}

-(void)sort
{
    NSSortDescriptor *sortByTitle = [NSSortDescriptor.alloc initWithKey:kTaskTitle
                                                              ascending:true];
    NSArray *originalTasks = self.tasks;
    NSArray *orderedTasks = [self.tasks sortedArrayUsingDescriptors:@[sortByTitle]];

    if ([originalTasks isEqualToArray:orderedTasks])
    {
        return;
    }
    self.tasks = orderedTasks;
    [self updateParentTaskIfPossible];

    NSArray<NSIndexPath *> *moveFrom;
    NSArray<NSIndexPath *> *moveTo;
    [orderedTasks movingIndexPathsFor:originalTasks
                             moveFrom:&moveFrom
                               moveTo:&moveTo];

    [self.tableView beginUpdates];
    [moveFrom enumerateObjectsUsingBlock:^(NSIndexPath * _Nonnull fromIndexPath, NSUInteger idx, BOOL * _Nonnull stop)
     {
         NSIndexPath *toIndexPath = moveTo[idx];
         [self.tableView moveRowAtIndexPath:fromIndexPath toIndexPath:toIndexPath];
     }];
    [self.tableView endUpdates];
}


#pragma mark - Data helpers

-(void)updateParentTaskIfPossible
{
    TSTask *parent = self.tasks.firstObject.parentTask;
    if (parent)
    {
        parent.childrenTasks = self.tasks;
    }
}

-(void)calculateMovableRows:(NSArray *)originals
                    objects:(NSArray *)objects
                   moveFrom:(NSArray<NSIndexPath *> *__autoreleasing*)moveFrom
                     moveTo:(NSArray<NSIndexPath *> *__autoreleasing*)moveTo
{
    NSMutableArray *pathsMoveFrom = NSMutableArray.new;
    NSMutableArray *pathsMoveTo = NSMutableArray.new;

    for (NSUInteger index = 0; index < objects.count; index++)
    {
        id object = objects[index];
        NSUInteger originalIndex = [originals indexOfObject:object];
        if (originalIndex != index)
        {
            [pathsMoveFrom addObject:[NSIndexPath indexPathForRow:originalIndex
                                                        inSection:0]];
            [pathsMoveTo addObject:[NSIndexPath indexPathForRow:index
                                                      inSection:0]];
        }
    }
    *moveFrom = pathsMoveFrom;
    *moveTo = pathsMoveTo;
}


#pragma mark - UI Helpers

-(void)setupToolbar
{
    UIBarButtonItem *completeAll = [UIBarButtonItem.alloc initWithTitle:NSLocalizedString(@"tasks_tb_complete_all", @"Tasks action to mark all not-completed task as complete")
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(completeAll)];
    UIBarButtonItem *sort = [UIBarButtonItem.alloc initWithTitle:NSLocalizedString(@"tasks_tb_sort_by_name", @"Tasks action to sort the tasks by name")
                                                           style:UIBarButtonItemStylePlain
                                                          target:self
                                                          action:@selector(sort)];

    self.toolbarItems = @[completeAll, sort];
}

-(void)setupTableView
{
    self.tableView.tableHeaderView = [UIView.alloc initWithFrame:CGRectZero];
    self.tableView.tableFooterView = [UIView.alloc initWithFrame:CGRectZero];
    [self.tableView registerClass:TaskCell.class
           forCellReuseIdentifier:cellIdentifier];
}

-(void)reloadRows:(NSArray<NSIndexPath *> *)rowsToReload
{
    [self.tableView performBatchUpdates:^
     {
         [self.tableView reloadRowsAtIndexPaths:rowsToReload
                               withRowAnimation:UITableViewRowAnimationFade];
     }
                             completion:nil];
}


@end
