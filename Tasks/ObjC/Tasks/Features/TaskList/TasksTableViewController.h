//
//  TasksTableViewController.h
//  Tasks
//
//  Copyright (c) 2013 Cultured Code. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TSTask.h"

NS_ASSUME_NONNULL_BEGIN


@interface TasksTableViewController : UITableViewController

-(instancetype)initWithTasks:(NSArray<TSTask *> *)tasks;


@end

NS_ASSUME_NONNULL_END
