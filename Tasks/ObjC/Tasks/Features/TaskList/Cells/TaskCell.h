//
//  TaskCell.h
//  Tasks
//
//  Copyright (c) 2013 Cultured Code. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TSTask.h"


@interface TaskCell : UITableViewCell

-(void)configureWith:(TSTask *)task;


@end
