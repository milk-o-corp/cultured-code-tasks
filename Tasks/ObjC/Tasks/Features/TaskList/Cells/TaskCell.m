//
//  TaskCell.m
//  Tasks
//
//  Copyright (c) 2013 Cultured Code. All rights reserved.
//


#import "TaskCell.h"


@interface TaskCell ()

@property (nonatomic, strong) UIView *vw_container;
@property (nonatomic, strong) UIImageView *ivw_checkmark;
@property (nonatomic, strong) UILabel *lbl_title;

@end

@implementation TaskCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
{
    if (!(self = [super initWithStyle:style
                      reuseIdentifier:reuseIdentifier]))
    {
        return nil;
    }
    [self setupViews];

    return self;
}

-(void)configureWith:(TSTask *)task
{
    self.accessoryType = (  task.childrenTasks.count > 0
                          ? UITableViewCellAccessoryDetailDisclosureButton
                          : UITableViewCellAccessoryNone);
    NSString *imageName = (  task.isCompleted
                           ? @"ic_checkbox_checked"
                           : @"ic_checkbox_empty");
    self.ivw_checkmark.image = [UIImage imageNamed:imageName];
    self.lbl_title.text = task.title;

    self.lbl_title.textColor = (  task.isCompleted
                                ? UIColor.lightGrayColor
                                : UIColor.blackColor);

    // Configure accessibility elements
    self.vw_container.accessibilityLabel = task.title;
    self.vw_container.accessibilityValue = NSLocalizedString((  task.isCompleted
                                                              ? @"tasks_list_element_checked"
                                                              : @"tasks_list_element_empty"), "Accessibility value for a Task");
}

-(void)setupViews
{
    UIView *containerView = UIView.new;
    containerView.translatesAutoresizingMaskIntoConstraints = false;
    containerView.backgroundColor = UIColor.clearColor;
    containerView.isAccessibilityElement = true;
    containerView.accessibilityTraits = UIAccessibilityTraitButton;
    containerView.accessibilityHint = NSLocalizedString(@"tasks_list_element_ac_hint", @"Accessibility hint for Task tap");
    self.vw_container = containerView;
    [self.contentView addSubview:containerView];

    UIImageView *imageView = [UIImageView.alloc initWithImage:[UIImage imageNamed:@"ic_checkbox_empty"]];
    imageView.translatesAutoresizingMaskIntoConstraints = false;
    self.ivw_checkmark = imageView;
    [containerView addSubview:imageView];

    UILabel *titleLabel = UILabel.new;
    titleLabel.translatesAutoresizingMaskIntoConstraints = false;
    // Using A Custom Font With Dynamic Type
    UIFont *font = [UIFont fontWithName:@"AmericanTypewriter"
                                   size:16.];
    UIFontMetrics *fontMetrics = [UIFontMetrics metricsForTextStyle:UIFontTextStyleBody];
    titleLabel.font = [fontMetrics scaledFontForFont:font];
    titleLabel.adjustsFontForContentSizeCategory = true;
    self.lbl_title = titleLabel;
    [containerView addSubview:titleLabel];

    [NSLayoutConstraint activateConstraints:@[
                                              // Container View
                                              [containerView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor],
                                              [containerView.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor],
                                              [containerView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor],
                                              [containerView.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor],
                                              // Checkmark ImageView
                                              [imageView.topAnchor constraintEqualToAnchor:containerView.topAnchor],
                                              [imageView.bottomAnchor constraintEqualToAnchor:containerView.bottomAnchor],
                                              [imageView.leadingAnchor constraintEqualToAnchor:containerView.leadingAnchor],
                                              [imageView.widthAnchor constraintEqualToConstant:40.],
                                              // Title Label
                                              [titleLabel.leadingAnchor constraintEqualToAnchor:imageView.trailingAnchor],
                                              [titleLabel.centerYAnchor constraintEqualToAnchor:containerView.centerYAnchor]
                                              ]];
}


@end
