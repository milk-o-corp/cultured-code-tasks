//
//  Task.m
//  Tasks
//
//  Copyright (c) 2013 Cultured Code. All rights reserved.
//


#import "TSTask.h"

#define NOW NSDate.new


@interface TSTask ()

@property (nonatomic, strong) NSMutableArray<TSTask *> *tasks;
@property (nonatomic, assign, getter=isCompleted, readwrite) BOOL completed;
@property (nonatomic, strong, readwrite) NSDate *modified;

@end

@implementation TSTask

-(instancetype)init
{
    if (!(self = [super init]))
    {
        return nil;
    }
    [self parseTitle:nil
           completed:false
            children:nil];

    return self;
}

-(instancetype)initWithTitle:(NSString *)title
{
    if (!(self = [super init]))
    {
        return nil;
    }
    [self parseTitle:title
           completed:false
            children:nil];

    return self;
}

-(instancetype)initWithTaskDescriptor:(NSDictionary *)descriptor
{
    if (!(self = [super init]))
    {
        return nil;
    }
    NSString *title = descriptor[kTaskTitle];
    BOOL completed = [descriptor[kTaskCompleted] boolValue];
    NSArray *childrenDescriptors = descriptor[kTaskChildrenTasks];

    NSMutableArray<TSTask *> *children = NSMutableArray.new;

    if (   childrenDescriptors
        && childrenDescriptors.count > 0)
    {
        for (NSDictionary *subDescriptor in childrenDescriptors)
        {
            TSTask *subTask = [TSTask.alloc initWithTaskDescriptor:subDescriptor];
            [children addObject:subTask];
        }
    }
    [self parseTitle:title
           completed:completed
            children:children];

    return self;
}

-(void)parseTitle:(NSString * _Nullable)title
        completed:(BOOL)completed
         children:(NSArray<TSTask *> * _Nullable)children
{
    self.title = (   title
                  ?: @"<no title>");
    self.completed = completed;
    if (children)
    {
        for (TSTask *child in children)
        {
            [self addChild:child];
        }
    }
    self.modified = NOW;
}


#pragma mark - Super overrides

-(BOOL)isEqual:(id)object
{
    if (object == nil)
    {
        return false;
    }
    if (![object isKindOfClass:self.class])
    {
        return false;
    }
    TSTask *task = object;

    return (   [self.title isEqualToString:task.title]
            && self.isCompleted == task.isCompleted
            && [self.childrenTasks isEqualToArray:task.childrenTasks]);
}

#pragma mark - Getter/Setter overrides

-(void)setTitle:(NSString *)title
{
    _title = title;
    self.modified = NOW;
}

-(NSArray<TSTask *> *)childrenTasks
{
    return self.tasks;
}

-(void)setChildrenTasks:(NSArray<TSTask *> *)childrenTasks
{
    self.tasks = childrenTasks.mutableCopy;
}


#pragma mark - Public helpers

-(void)toggleComplete
{
    self.modified = NOW;
    self.completed = !self.isCompleted;
}


#pragma mark - Child manipulation

-(void)addChild:(TSTask *)child
{
    child.parentTask = self;
    self.modified = NOW;
    [self.tasks addObject:child];
}

-(void)removeChild:(TSTask *)child
{
    [self.tasks removeObject:child];
}

-(void)makeAllChildrenComplete
{
    [TSTask makeComplete:self.childrenTasks];
}

-(void)deleteChildren
{
    [self.tasks removeAllObjects];
}


#pragma mark - Class methods

+(void)makeComplete:(NSArray<TSTask *> *)tasks
{
    if (tasks.count == 0)
    {
        return;
    }
    NSArray<TSTask *> *notCompletedTasks = [tasks filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.isCompleted == NO"]];
    if (notCompletedTasks.count == 0)
    {
        return;
    }
    for (TSTask *task in notCompletedTasks)
    {
        [task toggleComplete];
    }
}

+(NSArray<NSIndexPath *> *)notCompletedIndexPathsFrom:(NSArray<TSTask *> *)tasks
                                            inSection:(NSUInteger)section
{
    NSMutableArray<NSIndexPath *> *indexPaths = NSMutableArray.new;
    [tasks enumerateObjectsUsingBlock:^(TSTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
    {
        if (obj.isCompleted)
        {
            return;
        }
        NSIndexPath *path = [NSIndexPath indexPathForRow:idx
                                               inSection:section];
        [indexPaths addObject:path];
    }];
    return indexPaths;
}


#pragma mark - Lazy load

-(NSMutableArray<TSTask *> *)tasks
{
    if (_tasks)
    {
        return _tasks;
    }
    _tasks = NSMutableArray.new;

    return _tasks;
}


@end
