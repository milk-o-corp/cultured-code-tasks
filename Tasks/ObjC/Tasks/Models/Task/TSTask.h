//
//  Task.h
//  Tasks
//
//  Copyright (c) 2013 Cultured Code. All rights reserved.
//


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define kTaskTitle          @"title"
#define kTaskCompleted      @"completed"
#define kTaskChildrenTasks  @"childrenTasks"


@interface TSTask : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign, getter=isCompleted, readonly) BOOL completed;
@property (nonatomic, strong, readonly) NSDate *modified;

@property (nonatomic, strong) NSArray<TSTask *> *childrenTasks;
@property (nonatomic, weak, nullable) TSTask *parentTask;

-(instancetype)initWithTitle:(NSString *)title;
-(instancetype)initWithTaskDescriptor:(NSDictionary *)descriptor;

-(void)toggleComplete;

-(void)addChild:(TSTask *)child;
-(void)removeChild:(TSTask *)child;
-(void)makeAllChildrenComplete;
-(void)deleteChildren;
+(void)makeComplete:(NSArray<TSTask *> *)tasks;
+(NSArray<NSIndexPath *> *)notCompletedIndexPathsFrom:(NSArray<TSTask *> *)tasks
                                            inSection:(NSUInteger)section;

@end

NS_ASSUME_NONNULL_END
