# Cover letter

I have seen on Twitter that you are looking for **iOS/macOS Developer**. I found this opportunity interesting. On the one hand, because of your product, and on the other hand, because there was a task in the description _(and I really like this kind of attitude)_.

**What can you win when you choose me:**

- A really really enthusiastic & thorough problem-solver
- Not only a developer but also a fan of iOS development
- I like when there's order and I like to think and build the rule-based systems
- Up-to-date tech knowledge
- Prototyping and UX skills

**What is this for me:**

- A whole new challenge _(I am currently working for a big & multinational company)_
- Chance to develop _(finally)_ a <u>real & cool product</u>, with <u>real users</u> and <u>real problems</u>
- Learn from more experienced colleagues and share my knowledge & approaches
- Improve my knowledge of English in an inescapable way

I am working for Finastra Labs since 2014. Our innovation lab is a little team inside the big company to forge and invent ideas and workarounds for different problems. I have been started as a web-developer trainee, but from 2015 I am developing with the mobile team. 

Most of the time I am working on iOS prototypes but I had a few tasks with Google assistant, Watson AI, DialogFlow (Api.ai) and there are also two medium sized projects where I am responsible for the backend development.

Team-wide productivity improvements _(because they were not before and I missed them very)_:

- Flow improvements: CI (xcode server), OCLint, Uncrustify, Swiftlint, Code review 
- Git-policy (branch-naming, commit message format and frequency, etc.)
- iOS style-guide _(conventions)_

I am also a mentor to one of my colleagues who learns iOS from the beginning and helped other newcomers to start working with our projects _(the conventions and the project descriptions helped a lot)_.

I arranged 3 "Labs DevDays", and give presentations on different topics (Dev tools, Dev guides intro, Meetup summaries, Animations for devs, What is a commit, etc.). I am an active participant and award-winner of 2 Hackathon in Finastra. 

Last fall, I completed an out-of-worktime Design-Thinking 12 week long theoretical and practical training (http://momeid.mome.hu/). It was very different from my everyday life but totally worth it. I get new perspectives and also helped me to make decisions or to have better arguments in problematic cases. I have participated in several design-sprint is on.

Besides my everyday work, I took side-projects in AngularJS & PHP, in my CV you can read about them.

I love to be up-to-date in the tech world but mostly from the developer's side. I have a rich twitter-list and I read tweets daily (https://twitter.com/milk_o_man/lists), this is my main news source. I have a few favorite iOS developers, like John Sundell, Paul Hudson, Ole Begemann, Chris Eidhof, Florian Kugler and many others. I follow several newsletters: Swiftly Curated, Awesome iOS, Indie iOS Focus Weekly & iOSGoodies. 

I would like to join the team of Cultured Code(rs), where I can help in development with my expertise, with my attitude, and where I can raise my professional experience.

I look forward to your response.